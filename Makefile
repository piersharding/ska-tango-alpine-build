DOCKERFILE ?= Dockerfile
DOCKER_BUILD_ARGS ?= --build-arg BASE_IMAGE="python:3.8.10-alpine3.13" --build-arg CAR_PYPI_REPOSITORY_URL="https://artefact.skao.int/repository/pypi-all/simple"
NAME ?= ska-ex03-tango
TAG ?= 0.1.1
BASE = $(shell pwd)
THIS_HOST ?= $(shell (ip a 2> /dev/null || ifconfig) | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)

.DEFAULT_GOAL := help

.PHONY: reqs cppimage builderimage runtimeimage powersupplyimage images clean image start tangodb tangods tangops stop stopdb stopds stopps jive help

# define private rules and additional makefiles
-include PrivateRules.mak

# The image names
CPPTANGO_IMG = $(NAME)-cpptango
TANGOBUILER_IMG = $(NAME)-builder
TANGORUNTIME_IMG = $(NAME)-runtime
TANGOPS_IMG = $(NAME)-powersupply

# The test container names
TANGO_DB = tangodb
TANGO_DS = tangods
TANGO_PS = powersupply

cppimage: ## Build cppTango image with podman
	docker build --pull $(DOCKER_BUILD_ARGS) -f Dockerfile.cpptango --tag "$(CPPTANGO_IMG):$(TAG)" .

builderimage: ## Build builder image with podman
	docker build --build-arg BASE_IMAGE="$(CPPTANGO_IMG):$(TAG)" -f Dockerfile.builder --tag "$(TANGOBUILER_IMG):$(TAG)" .

runtimeimage: ## Build runtime image with podman
	docker build --build-arg BUILDER_BASE_IMAGE="$(TANGOBUILER_IMG):$(TAG)" --build-arg BASE_IMAGE="$(CPPTANGO_IMG):$(TAG)" -f Dockerfile.runtime --tag "$(TANGORUNTIME_IMG):$(TAG)" .

powersupplyimage: ## Build powersupply image with podman
	docker build --build-arg BASE_IMAGE="$(TANGORUNTIME_IMG):$(TAG)" -f Dockerfile.powersupply --tag "$(TANGOPS_IMG):$(TAG)" .

images: cppimage builderimage runtimeimage powersupplyimage  ## Build all images

tangodb: stopdb  ## launch the tangodb in Docker
	docker run --name $(TANGO_DB) --net=host \
		--env MYSQL_ROOT_PASSWORD=secret \
		--env MYSQL_DATABASE=tango \
		--env MYSQL_USER=tango \
		--env MYSQL_PASSWORD=tango \
		--env MYSQL_ALLOW_EMPTY_PASSWORD=1 \
		-d artefact.skao.int/ska-tango-images-tango-db:10.4.11

stopdb:  ## stop the tangodb
	docker inspect $(TANGO_DB) >/dev/null 2>&1 && docker rm -f $(TANGO_DB) || true

tangods: stopds  ## launch the tangods in Docker
	docker run --name $(TANGO_DS) --net=host \
		--env MYSQL_HOST=$(THIS_HOST):3306 \
		--env MYSQL_USER=tango \
		--env MYSQL_PASSWORD=tango \
		--env MYSQL_DATABASE=tango \
		-d artefact.skao.int/ska-tango-images-tango-cpp:9.3.5 \
		/usr/local/bin/DataBaseds "2" -ORBendPoint giop:tcp::10000

stopds:  ## stop the tangods
	docker inspect $(TANGO_DS) >/dev/null 2>&1 && docker rm -f $(TANGO_DS) || true

tangops: stopps  ## launch the powersupply in Docker
	docker run --name $(TANGO_PS) --net=host \
		-e TANGO_HOST=$(THIS_HOST):10000 \
		-d $(TANGOPS_IMG):$(TAG)

stopps:  ## stop the powersupply
	docker inspect $(TANGO_PS) >/dev/null 2>&1 && docker rm -f $(TANGO_PS) || true

start:  ## Start all the containers
	make tangodb
	sleep 15
	make tangods
	sleep 3
	make tangops

stop: stopps stopds stopdb  ## Stop all containers
	docker ps -a

logs:  ## all container logs
	docker ps
	docker inspect $(TANGO_DB) >/dev/null 2>&1 && docker logs $(TANGO_DB) || true
	docker inspect $(TANGO_DS) >/dev/null 2>&1 && docker logs $(TANGO_DS) || true
	docker inspect $(TANGO_PS) >/dev/null 2>&1 && docker logs $(TANGO_PS) || true

jive:  ## boot the Jive client in Docker
	docker run --rm --name jive --net host --ipc host --pid host --privileged \
		-v /dev:/dev \
		-v /etc/passwd:/etc/passwd:ro \
	--user=$$(id -u) -v $${HOME}:$${HOME} -w $${HOME} \
	-e DISPLAY=unix$$DISPLAY -e HOME=$${HOME} --env TANGO_HOST=$(THIS_HOST):10000 -ti tangocs/tango-jive:latest

help:  ## show this help.
	@grep -hE '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
